//
//  IEButtonModel.h
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum : NSUInteger {
    IEButtonTypeTop,
    IEButtonTypeLeft,
    IEButtonTypeRight,
} IEButtonType;

@interface IEButtonModel : NSObject

@property (nonatomic, assign) IEButtonType type;
@property (nonatomic, strong) NSString * name;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
