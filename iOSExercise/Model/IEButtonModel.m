//
//  IEButtonModel.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IEButtonModel.h"

NSString * const dicTypeKey = @"type";
NSString * const dicNameKey = @"name";

@implementation IEButtonModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary {
    
    self = [super init];
    
    if (self) {
        
        NSString * strType = [dictionary valueForKey:dicTypeKey];
        if ([strType isEqualToString:@"left"]) {
            _type = IEButtonTypeLeft;
        } else if ([strType isEqualToString:@"right"]) {
            _type = IEButtonTypeRight;
        } else {
            _type = IEButtonTypeTop;
        }
        
        _name = [dictionary valueForKey:dicNameKey];
    }
    
    return self;
}

@end
