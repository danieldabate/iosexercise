//
//  IEInterfaceService.h
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^InterfaceElementsCallback)(NSArray * elements);

@interface IEInterfaceService : NSObject

+ (IEInterfaceService *)sharedService;

- (void)getInterfaceElements:(InterfaceElementsCallback) callback;

@end
