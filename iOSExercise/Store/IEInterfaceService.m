//
//  IEInterfaceService.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IEInterfaceService.h"
#import "IEButtonModel.h"

NSString * const dicInterfaceElementsKey = @"elements";

@implementation IEInterfaceService

+ (IEInterfaceService *)sharedService {
    
    static IEInterfaceService * service = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        service = [IEInterfaceService new];
    });
    
    return service;
}

- (void)getInterfaceElements:(InterfaceElementsCallback) callback {

    // Get data from interface service
    NSString * dataFilePath = [[NSBundle mainBundle] pathForResource:@"InterfaceServiceData" ofType:@"json"];
    NSData * interfaceServiceData = [NSData dataWithContentsOfFile:dataFilePath];
    
    // Get buttons from interface service data
    NSDictionary * interfaceElementsDictionary = [NSJSONSerialization JSONObjectWithData:interfaceServiceData options:NSJSONReadingAllowFragments error:nil];
    
    NSArray * interfaceElementsDictionaryList = [interfaceElementsDictionary objectForKey:dicInterfaceElementsKey];
    
    NSMutableArray * interfaceElementsList = [NSMutableArray array];
    for (NSDictionary * interfaceElementDictionary in interfaceElementsDictionaryList) {
        if ([interfaceElementDictionary[@"element"] isEqualToString:@"button"]) {
            IEButtonModel * buttonModel = [[IEButtonModel alloc] initWithDictionary:interfaceElementDictionary];
            [interfaceElementsList addObject: buttonModel];
        }
    }
    
    callback(interfaceElementsList);
}

@end
