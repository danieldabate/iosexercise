//
//  main.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
