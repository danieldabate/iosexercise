//
//  IEMainTableViewController.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IEMainTableViewController.h"

#import "IEButtonModel.h"
#import "IEInterfaceService.h"
#import "IETableViewButtonCell.h"
#import "IETableViewButtonLeftCell.h"
#import "IETableViewButtonRightCell.h"
#import "IETableViewButtonTopCell.h"

@interface IEMainTableViewController ()

@property (nonatomic, copy) NSArray * buttonModelsList;

@end

@implementation IEMainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Load data from backend
    [[IEInterfaceService sharedService] getInterfaceElements:^(NSArray *elements) {
        self.buttonModelsList = elements;
        [self.tableView reloadData];
    }];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.buttonModelsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IEButtonModel * buttonModel = [self.buttonModelsList objectAtIndex:indexPath.section];
    
    NSString * cellReuseIdentifier = [[IETableViewButtonCell classForButtonType:buttonModel.type] reuseIdentifier];
    IETableViewButtonCell * cell = [self.tableView dequeueReusableCellWithIdentifier: cellReuseIdentifier];

    cell.delegate = self;
    
    cell.titleView.text = buttonModel.name;
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    IEButtonModel * buttonModel = [self.buttonModelsList objectAtIndex:indexPath.section];
    
    return [[IETableViewButtonCell classForButtonType:buttonModel.type] suggestedHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    
    if (section == 0) {
        return 22;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 22;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [UIView new];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [UIView new];
}

#pragma mark - IETableViewButtonCellDelegate
- (void)didSelectTableViewButtonCell:(IETableViewButtonCell *)cell {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:cell.titleView.text message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Close" style:UIAlertActionStyleDefault handler:nil]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
