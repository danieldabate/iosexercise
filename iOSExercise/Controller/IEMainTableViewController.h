//
//  IEMainTableViewController.h
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IETableViewButtonCell.h"

@interface IEMainTableViewController : UITableViewController <IETableViewButtonCellDelegate>


@end

