//
//  AppDelegate.h
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

