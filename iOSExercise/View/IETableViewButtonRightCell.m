//
//  IETableViewButtonRightCell.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/4/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IETableViewButtonRightCell.h"

@implementation IETableViewButtonRightCell

+ (NSString *)reuseIdentifier {
    return @"IETableViewButtonRightCell";
}

+ (CGFloat)suggestedHeight {
    return 70;
}

@end
