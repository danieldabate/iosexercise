//
//  IETableViewButtonTopCell.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/4/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IETableViewButtonTopCell.h"

@implementation IETableViewButtonTopCell

+ (NSString *)reuseIdentifier {
    return @"IETableViewButtonTopCell";
}

+ (CGFloat)suggestedHeight {
    return 110;
}

@end
