//
//  IETableViewButtonCell.h
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IEButtonModel.h"

@protocol IETableViewButtonCellDelegate;

@interface IETableViewButtonCell : UITableViewCell

@property (nonatomic, weak) id <IETableViewButtonCellDelegate> delegate;

@property (nonatomic, readonly, weak) IBOutlet UILabel *titleView;;

+ (Class)classForButtonType:(IEButtonType)type;

+ (NSString *)reuseIdentifier;
+ (CGFloat)suggestedHeight;

@end

@protocol IETableViewButtonCellDelegate <NSObject>

@optional
- (void)didSelectTableViewButtonCell:(IETableViewButtonCell *)cell;

@end
