//
//  IETableViewButtonCell.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/3/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IETableViewButtonCell.h"

#import "IETableViewButtonLeftCell.h"
#import "IETableViewButtonRightCell.h"
#import "IETableViewButtonTopCell.h"

#import "IEButtonModel.h"

@interface IETableViewButtonCell ()

@property (nonatomic, weak) IBOutlet UIButton *buttonView;
@property (nonatomic, weak) IBOutlet UILabel *titleView;

@end

@implementation IETableViewButtonCell

+ (Class)classForButtonType:(IEButtonType)type {
    switch (type) {
        case IEButtonTypeLeft:
            return [IETableViewButtonLeftCell class];
            
        case IEButtonTypeRight:
            return [IETableViewButtonRightCell class];
            
        case IEButtonTypeTop:
            return [IETableViewButtonTopCell class];
    }
}

+ (NSString *)reuseIdentifier {
    // Should override
    return @"";
}

+ (CGFloat)suggestedHeight {
    // Should override
    return 0;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.buttonView.backgroundColor = [UIColor colorWithRed:0.0 green:122.0/255.0 blue:1.0 alpha:1.0];
    [self.buttonView setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    self.buttonView.contentEdgeInsets = UIEdgeInsetsMake(7, 15, 7, 15);
    self.buttonView.layer.cornerRadius = 8.0f;
}

- (IBAction)buttonPressed:(id)sender {
    if ([self.delegate respondsToSelector:@selector(didSelectTableViewButtonCell:)]) {
        [self.delegate didSelectTableViewButtonCell:self];
    }
}

@end
