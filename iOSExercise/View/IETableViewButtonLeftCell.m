//
//  IETableViewButtonLeftCell.m
//  iOSExercise
//
//  Created by Daniel D'Abate on 3/4/17.
//  Copyright © 2017 Daniel D'Abate. All rights reserved.
//

#import "IETableViewButtonLeftCell.h"

@implementation IETableViewButtonLeftCell

+ (NSString *)reuseIdentifier {
    return @"IETableViewButtonLeftCell";
}

+ (CGFloat)suggestedHeight {
    return 70;
}

@end
